package com.ddcat.constant;

/**
 * 缓存的key 常量
 *
 * @author dd-cat
 */
public interface CacheConstants {
    /**
     * 字典
     */
    String DICT = "dict:";
    /**
     * 角色菜单
     */
    String ROLE_MENU = "role:menu:";
}
